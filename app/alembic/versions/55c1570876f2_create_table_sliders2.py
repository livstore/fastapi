"""create table sliders2

Revision ID: 55c1570876f2
Revises: 
Create Date: 2024-01-24 11:45:49.691057

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '55c1570876f2'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('infoblocks',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_infoblocks_name'), 'infoblocks', ['name'], unique=True)
    op.create_table('infoblocks_lang',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('infoblocks_id', sa.Integer(), nullable=True),
    sa.Column('lang_id', sa.Integer(), nullable=True),
    sa.Column('htmlblock', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['infoblocks_id'], ['infoblocks.id'], ),
    sa.ForeignKeyConstraint(['lang_id'], ['langs.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_infoblocks_lang_infoblocks_id'), 'infoblocks_lang', ['infoblocks_id'], unique=False)
    op.create_table('items',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.Column('slug', sa.String(length=100), nullable=True),
    sa.Column('price', sa.Float(), nullable=True),
    sa.Column('foto', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_items_name'), 'items', ['name'], unique=True)
    op.create_index(op.f('ix_items_slug'), 'items', ['slug'], unique=True)
    op.create_table('items_lang',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('items_id', sa.Integer(), nullable=True),
    sa.Column('lang_id', sa.Integer(), nullable=True),
    sa.Column('title', sa.Text(), nullable=True),
    sa.Column('about', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['items_id'], ['items.id'], ),
    sa.ForeignKeyConstraint(['lang_id'], ['langs.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_items_lang_items_id'), 'items_lang', ['items_id'], unique=False)
    op.create_table('sliders',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_sliders_name'), 'sliders', ['name'], unique=True)
    op.create_table('sliders_lang',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('sliders_id', sa.Integer(), nullable=True),
    sa.Column('lang_id', sa.Integer(), nullable=True),
    sa.Column('text', sa.Text(), nullable=True),
    sa.Column('photo', sa.String(length=100), nullable=True),
    sa.ForeignKeyConstraint(['lang_id'], ['langs.id'], ),
    sa.ForeignKeyConstraint(['sliders_id'], ['sliders.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_sliders_lang_sliders_id'), 'sliders_lang', ['sliders_id'], unique=False)
    op.create_table('pages',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=100), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_pages_name'), 'pages', ['name'], unique=True)
    op.create_table('pages_lang',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('pages_id', sa.Integer(), nullable=True),
    sa.Column('lang_id', sa.Integer(), nullable=True),
    sa.Column('htmlblock', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['lang_id'], ['langs.id'], ),
    sa.ForeignKeyConstraint(['pages_id'], ['pages.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_pages_lang_pages_id'), 'pages_lang', ['pages_id'], unique=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_pages_lang_pages_id'), table_name='pages_lang')
    op.drop_table('pages_lang')
    op.drop_index(op.f('ix_pages_name'), table_name='pages')
    op.drop_table('pages')
    op.drop_index(op.f('ix_sliders_lang_sliders_id'), table_name='sliders_lang')
    op.drop_table('sliders_lang')
    op.drop_index(op.f('ix_sliders_name'), table_name='sliders')
    op.drop_table('sliders')
    op.drop_index(op.f('ix_items_lang_items_id'), table_name='items_lang')
    op.drop_table('items_lang')
    op.drop_index(op.f('ix_items_slug'), table_name='items')
    op.drop_index(op.f('ix_items_name'), table_name='items')
    op.drop_table('items')
    op.drop_index(op.f('ix_infoblocks_lang_infoblocks_id'), table_name='infoblocks_lang')
    op.drop_table('infoblocks_lang')
    op.drop_index(op.f('ix_infoblocks_name'), table_name='infoblocks')
    op.drop_table('infoblocks')
    # ### end Alembic commands ###
