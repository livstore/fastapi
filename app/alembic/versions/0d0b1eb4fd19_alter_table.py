"""alter  table 

Revision ID: 0d0b1eb4fd19
Revises: 55c1570876f2
Create Date: 2024-01-25 20:10:40.031308

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '0d0b1eb4fd19'
down_revision: Union[str, None] = '55c1570876f2'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('catalogs', sa.Column('photo', sa.String(length=100), nullable=True))
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('catalogs', 'photo')
    # ### end Alembic commands ###
