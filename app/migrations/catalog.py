import sqlalchemy
from .lang import lang_table


metadata = sqlalchemy.MetaData()


catalog_table = sqlalchemy.Table(
    "catalogs",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("slug", sqlalchemy.String(100)),
    sqlalchemy.Column("parentid", sqlalchemy.Integer, default=0),
    sqlalchemy.Column("photo", sqlalchemy.String(100), nullable=True),
)

catalog_lang_table = sqlalchemy.Table(
    "catalogs_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("catalogs_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("catalogs.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
)
