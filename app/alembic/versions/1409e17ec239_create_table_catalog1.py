"""create table catalog1

Revision ID: 1409e17ec239
Revises: 8b6ba83e59c6
Create Date: 2024-01-27 23:00:23.442199

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision: str = '1409e17ec239'
down_revision: Union[str, None] = '8b6ba83e59c6'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('items', sa.Column('catalog_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'items', 'catalogs', ['catalog_id'], ['id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'items', type_='foreignkey')
    op.drop_column('items', 'catalog_id')
    # ### end Alembic commands ###
