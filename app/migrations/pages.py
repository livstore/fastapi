import sqlalchemy
from .lang import lang_table


metadata = sqlalchemy.MetaData()


page_table = sqlalchemy.Table(
    "pages",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
)

page_lang_table = sqlalchemy.Table(
    "pages_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("pages_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("pages.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("htmlblock", sqlalchemy.Text, nullable=True),
)
