from typing import Union
from fastapi import Depends, FastAPI, HTTPException, File, UploadFile, Form
from fastapi.responses import JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordBearer
from fastapi.staticfiles import StaticFiles
from databases import Database
from passlib.context import CryptContext
from jose import jwt
from .models.Users import UserLogin,UserReturn,UserCreate
from datetime import datetime

import json
import shutil
import os




app = FastAPI()
app.mount("/images", StaticFiles(directory="/code/app/images"), name="images")


### START CONFIG CORS
origins = [
    "http://127.0.0.1",
    "http://127.0.0.1:3000",
    "http://127.0.0.1:8022",
    "http://localhost",
    "http://192.2.3.1",
    "http://localhost:3000",
    "http://localhost:8022",
    "http://127.0.0.1:8237",
    "http://localhost:8237",
    "http://adminrmebel.rfpgu.ru"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
### END CONFIG CORS

### START CONFIG DATABASE
DATABASE_URL = "postgresql://postgres:postgres@db_api_shop/apidb"
database = Database(DATABASE_URL)
@app.on_event("startup")
async def startup_database():
    await database.connect()
@app.on_event("shutdown")
async def shutdown_database():
    await database.disconnect()
### END  CONFIG DATABASE


## start CRYPTO
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
# Configure JWT settings
SECRET_KEY = "mysecretkey"
ALGORITHM = "HS256"
#получаем пароль
def hash_pass(password:str):
    return pwd_context.hash(password)
#генерируем токен
def create_jwt_token(data: dict):
    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)

async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
       payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])  # декодируем токен
       return payload.get("sub")  # тут мы идем в полезную нагрузку JWT-токена и возвращаем утверждение о юзере (subject); обычно там еще можно взять "iss" - issuer/эмитент, или "exp" - expiration time - время 'сгорания' и другое, что мы сами туда кладем
    except jwt.ExpiredSignatureError:
       pass  # тут какая-то логика ошибки истечения срока действия токена
    except jwt.InvalidTokenError:
       pass
def get_user(email: str):
    query = "SELECT *  FROM users WHERE email=:email"
    values = {"email":email}
    db_users = database.fetch_one(query=query, values=values)
    return db_users
## end  CRYPTI

#Main
@app.get("/")
def read_root():
    return {"value": "ApiShop"}


# Register User / Регистрация пользователя
@app.post("/register", response_model=UserReturn)
async def create_user(user: UserCreate):
    query = "INSERT INTO users (name, email, hashed_password) VALUES (:username, :email, :hashed_password) RETURNING id"
    values = {"username": user.name, "email": user.email, "hashed_password": hash_pass(user.hashed_password)}
    try:
        user_id = await database.execute(query=query, values=values)
        return {**user.dict(), "id": user_id}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to create user")


#Auth login user / Авторизация пользователя и ворзврашение токена
@app.post("/login")
async def post_login(user: UserLogin):
    query = "SELECT *  FROM users WHERE email=:email"
    values = { "email": user.email}
    try:
        db_users = await database.fetch_one(query=query, values=values)
        is_verified = pwd_context.verify(user.hashed_password, db_users.hashed_password)
        if is_verified:
            return {"token": create_jwt_token({"sub": db_users.email}), "token_type": "bearer"}
        else:
            raise HTTPException(status_code=500)
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed  login")

#ПОлучаем все данные записи
@app.post("/{tablename}/list/")
async def post_list_data(tablename,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    query = "SELECT * FROM "+tablename
    values={}
    projects_user =await database.fetch_all(query=query, values=values)
    if projects_user:
        return projects_user
    return {"error": "Records not found"}


#Добавление новой записи
@app.post("/{tablename}/store/")
async def store_list_data(tablename,data: dict,current_user: str = Depends(get_user_from_token)):

    user = await  get_user(current_user)
    strKey = ""
    strValue=""
    valueinsert_string = ""

    for key in data:
        strKey+=key+","
        strValue+=":"+key+","
        if (type(data[key]) == str):
            valueinsert_string += '"' + key + '":"' + data[key] + '",'
        if (type(data[key]) == int):
            valueinsert_string += '"' + key + '":' + str(data[key]) + ','

    strKey = strKey[:-1]
    strValue = strValue[:-1]
    valueinsert_string = valueinsert_string[:-1]
    valueinsert_string="{"+valueinsert_string+"}"
    json_object = json.loads(valueinsert_string)
    query = "INSERT INTO "+tablename+" ("+strKey+") VALUES ("+strValue+") RETURNING id"
    print(query)
    try:
        record_id = await database.execute(query=query, values=json_object)
        return { "id": record_id}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to create record")

#Удаление записи  по ID
@app.post("/{tablename}/delete/")
async def delete_id_data(tablename, data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    recordid=data['id']
    query = "DELETE FROM "+tablename+" WHERE id = :id RETURNING id"
    values = {"id":recordid}
    try:
        deleted_rows = await database.execute(query=query, values=values)
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to delete record from database")
    if deleted_rows:
        return {"message": "Record deleted successfully"}
    else:
        raise HTTPException(status_code=404, detail="Record not found")

# Получаем данные записи по ID
@app.post("/{tablename}/getid/{id}")
async def get_data_id(tablename:str,id:int,data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    strKey="id,"
    for key in data['datacolumn']:
        strKey+=key['column']+","
    strKey = strKey[:-1]
    query = "SELECT "+strKey+" FROM "+tablename+" WHERE id=:id"
    values = {"id": id}
    datarecordid =await database.fetch_all(query=query, values=values)
    if data['formlang']:
       querylang = "SELECT * FROM langs"
       datalangs = await database.fetch_all(query=querylang)
    if datarecordid :
        if data['formlang'] and datalangs:
            return {"data":datarecordid,"datalang":datalangs}
        else:
            return {"data": datarecordid, "datalang": {}}
    return {"error": "Records not found"}

@app.post("/{tablename}/getid_lang/")
async def get_data_id(tablename:str,data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    strKey="id,"
    for key in data['formlang']:
        strKey+=key['column']+","
    strKey = strKey[:-1]
    query = "SELECT "+strKey+" FROM "+tablename+"_lang WHERE lang_id=:langid and "+tablename+"_id=:recordid"
    print(query)
    values = {"langid": data['langid'],"recordid": data['recordid']}
    datarecordid =await database.fetch_all(query=query, values=values)
    if datarecordid :
       return {"data": datarecordid}

    return {"error": "Records not found","data": []}

#Изменение данных
@app.post("/{tablename}/update/")
async def update_id_data(tablename, data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    strKey = ""
    valueinsert_string = ""
    for key in data:
        if key!="id":
            strKey += key+" = :"+key+", "
            print(key)
            print(type(data[key]))
            if(type(data[key])== str):
                valueinsert_string += '"' + key + '":"' + data[key] + '",'
            if (type(data[key])== int):
                valueinsert_string += '"' + key + '":' + str(data[key]) + ','
    strKey = strKey[:-2]
    valueinsert_string = valueinsert_string[:-1]
    valueinsert_string = "{" + valueinsert_string + "}"
    json_object = json.loads(valueinsert_string)
    query = "UPDATE "+tablename+" SET  "+strKey+" WHERE id = "+str(data['id'])
    try:
        await database.execute(query=query, values=json_object)
        return {"status": "ok"}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to update record in database")




@app.post("/{tablename}/storelang/")
async def store_lang_data(tablename,data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    strKey = ""
    strValue=""
    valueinsert_string = ""

    for key in data:
        if key != "id":
            strKey+=key+","
            strValue+=":"+key+","
            if (type(data[key]) == str):
                valueinsert_string += '"' + key + '":"' + data[key] + '",'
            if (type(data[key]) == int):
                valueinsert_string += '"' + key + '":' + str(data[key]) + ','

    strKey = strKey[:-1]
    strValue = strValue[:-1]
    valueinsert_string = valueinsert_string[:-1]
    valueinsert_string="{"+valueinsert_string+"}"
    json_object = json.loads(valueinsert_string)
    query = "INSERT INTO "+tablename+"_lang ("+strKey+") VALUES ("+strValue+") RETURNING id"
    try:
        record_id = await database.execute(query=query, values=json_object)
        return { "id": record_id}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to create record")

@app.post("/{tablename}/updatellang/")
async def update_lang_data(tablename, data: dict,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    strKey = ""
    valueinsert_string = ""
    for key in data:
        if key!="id":
            strKey += key+" = :"+key+", "
            print(key)
            print(type(data[key]))
            if(type(data[key])== str):
                valueinsert_string += '"' + key + '":"' + data[key] + '",'
            if (type(data[key])== int):
                valueinsert_string += '"' + key + '":' + str(data[key]) + ','
    strKey = strKey[:-2]
    valueinsert_string = valueinsert_string[:-1]
    valueinsert_string = "{" + valueinsert_string + "}"
    json_object = json.loads(valueinsert_string)
    query = "UPDATE "+tablename+"_lang SET  "+strKey+" WHERE id = "+str(data['id'])
    try:
        await database.execute(query=query, values=json_object)
        return {"status": "ok"}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to update record in database")


@app.post("/{tablename}/fileupload_lang/")
async def update_file_lang_data(tablename,
                                recordid: str = Form(...),
                                langid: str = Form(...),
                                column: str = Form(...),
                                idl: str = Form(...),
                                file: UploadFile = File(...),
                                current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    try:
        # Specify the directory where you want to save the uploaded files
        UPLOAD_DIR = "/code/app/images/"+tablename
        # Create the directory if it doesn't exist
        os.makedirs(UPLOAD_DIR, exist_ok=True)
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        file_extension = os.path.splitext(file.filename)
        # Concatenate the upload directory and the filename
        newName=timestamp+file_extension[1]
        file_path = os.path.join(UPLOAD_DIR,newName)
        # Save the file to the specified directory
        with open(file_path, "wb") as f:
            shutil.copyfileobj(file.file, f)
            if int(idl)>0:
                query = "UPDATE " + tablename + "_lang SET "+column+"=:newname   WHERE id = " + idl
                values = {"newname":newName}
            else:
                query = "INSERT INTO " + tablename + "_lang (lang_id, "+tablename+"_id, "+column+") VALUES (:lang_id, :parent_id, :newName) RETURNING id"
                values = {"lang_id": int(langid),"parent_id":int(recordid),"newName":newName}
            await database.execute(query=query, values=values)
        return JSONResponse(content={"message": "File uploaded successfully", "newname": newName})
    except Exception as e:
        return JSONResponse(content={"message": "Error uploading file", "error": str(e)}, status_code=500)


@app.post("/{tablename}/fileupload/")
async def update_file_lang_data(tablename,
                                recordid: str = Form(...),
                                column: str = Form(...),
                                file: UploadFile = File(...),
                                current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    try:
        # Specify the directory where you want to save the uploaded files
        UPLOAD_DIR = "/code/app/images/"+tablename
        # Create the directory if it doesn't exist
        os.makedirs(UPLOAD_DIR, exist_ok=True)
        timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
        file_extension = os.path.splitext(file.filename)
        # Concatenate the upload directory and the filename
        newName=timestamp+file_extension[1]
        file_path = os.path.join(UPLOAD_DIR,newName)
        # Save the file to the specified directory
        with open(file_path, "wb") as f:
            shutil.copyfileobj(file.file, f)

            query = "UPDATE " + tablename + " SET "+column+"=:newname   WHERE id = " + recordid
            values = {"newname":newName}

            await database.execute(query=query, values=values)
        return JSONResponse(content={"message": "File uploaded successfully", "newname": newName})
    except Exception as e:
        return JSONResponse(content={"message": "Error uploading file", "error": str(e)}, status_code=500)


@app.post("/{tablename}/list_value/")
async def post_list_data_value(tablename,current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    query = "SELECT * FROM "+tablename
    values={}
    result =await database.fetch_all(query=query, values=values)
    resultArray = []
    if result:
        for val in result:
            query = "SELECT id,name as text FROM " + tablename+"_value WHERE "+tablename+"_id="+str(val['id'])
            resultvalue = await database.fetch_all(query=query, values={})
            resultArray.append({"id": val['id'],
                                "name": val['name'],
                                "resultArrayValye":resultvalue
                                })
        return resultArray
    return {"error": "Records not found"}




@app.post("/{tablename}/update_value/")
async def post_update_data_value(tablename,
                         data: dict,
                         current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    query = "DELETE FROM specifications_items WHERE items_id = :id  and specifications_id = :specifications_id RETURNING id"
    values = {"id": data['recordid'],"specifications_id":data['specifications_id']}
    await database.execute(query=query, values=values)
    query = "INSERT INTO specifications_items (specifications_id,items_id,specifications_value_id) VALUES (:specifications_id, :items_id,:specifications_value_id) RETURNING id"
    for datainsert in data['dataupdate']:
        values = {"items_id": data['recordid'],
                  "specifications_value_id":int(datainsert),
                  "specifications_id": data['specifications_id'],}
        await database.execute(query=query, values=values)
    return {"error": "Records not found"}

@app.post("/{tablename}/get_value_ids/")
async def post_update_data_value(tablename,
                         data: dict,
                         current_user: str = Depends(get_user_from_token)):
    user = await  get_user(current_user)
    query = "SELECT specifications_id FROM  specifications_items  WHERE items_id="+str(data['recordid'])+" group by specifications_id  "

    values = {}
    result = await database.fetch_all(query=query, values=values)
    resultArray = []
    if result:
        for val in result:
            query = "SELECT specifications_value_id FROM specifications_items WHERE specifications_id=" + str(val['specifications_id'])+" and  items_id="+str(data['recordid'])
            resultvalue = await database.fetch_all(query=query, values={})
            resultvalueGet=[]
            for KeyJ in resultvalue:
                resultvalueGet.append(KeyJ['specifications_value_id'])
            resultArray.append({"id": val['specifications_id'],
                                "idsValue": resultvalueGet
                                })
        return resultArray
    return {"error": "Records not found"}


