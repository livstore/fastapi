import sqlalchemy
from .lang import lang_table


metadata = sqlalchemy.MetaData()


slider_table = sqlalchemy.Table(
    "sliders",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
)

slider_lang_table = sqlalchemy.Table(
    "sliders_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("sliders_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("sliders.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("text", sqlalchemy.Text, nullable=True),
    sqlalchemy.Column("photo", sqlalchemy.String(100), nullable=True),
)
