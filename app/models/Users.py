from pydantic import BaseModel
from typing import Optional

class Users(BaseModel):
    id: int
    email: str
    name: str
    is_admin: int
    is_active: int


class UserReturn(BaseModel):
    name: str
    email: str
    id: Optional[int] = None

class UserLogin(BaseModel):
    email: str
    hashed_password: str

class UserCreate(BaseModel):
    name: str
    email: str
    hashed_password : str