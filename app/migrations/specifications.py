import sqlalchemy
from .lang import lang_table
from .items import item_table

metadata = sqlalchemy.MetaData()


specification_table = sqlalchemy.Table(
    "specifications",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
)

specification_lang_table = sqlalchemy.Table(
    "specifications_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("specifications_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("specifications.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
)

specification_value_table = sqlalchemy.Table(
    "specifications_value",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("specifications_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("specifications.id"),index=True),
    sqlalchemy.Column("name", sqlalchemy.String(100)),
)

specification_item_table = sqlalchemy.Table(
    "specifications_items",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("specifications_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("specifications.id"),index=True),
    sqlalchemy.Column("specifications_value_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("specifications_value.id"),index=True),
    sqlalchemy.Column("items_id", sqlalchemy.ForeignKey(item_table.c.id)),
)
