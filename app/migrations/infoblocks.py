import sqlalchemy
from .lang import lang_table


metadata = sqlalchemy.MetaData()


infoblock_table = sqlalchemy.Table(
    "infoblocks",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
)

infoblock_lang_table = sqlalchemy.Table(
    "infoblocks_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("infoblocks_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("infoblocks.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("htmlblock", sqlalchemy.Text, nullable=True),
)
