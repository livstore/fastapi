import sqlalchemy
from .lang import lang_table
from .catalog import catalog_table

metadata = sqlalchemy.MetaData()


item_table = sqlalchemy.Table(
    "items",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("slug", sqlalchemy.String(100), unique=True, index=True),
    sqlalchemy.Column("price", sqlalchemy.types.Float ),
    sqlalchemy.Column("catalog_id", sqlalchemy.ForeignKey(catalog_table.c.id),nullable=True),
    sqlalchemy.Column("foto", sqlalchemy.String(100), nullable=True),
)

item_lang_table = sqlalchemy.Table(
    "items_lang",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("items_id", sqlalchemy.Integer, sqlalchemy.ForeignKey("items.id"),index=True),
    sqlalchemy.Column("lang_id", sqlalchemy.ForeignKey(lang_table.c.id)),
    sqlalchemy.Column("title", sqlalchemy.Text, nullable=True),
    sqlalchemy.Column("about", sqlalchemy.Text, nullable=True),
)
